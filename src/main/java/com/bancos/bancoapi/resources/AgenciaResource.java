package com.bancos.bancoapi.resources;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancos.bancoapi.exception.RegistroNaoEncontrado;
import com.bancos.bancoapi.models.Agencia;
import com.bancos.bancoapi.repository.AgenciaRepository;

@RestController
@RequestMapping(value="/api/agencia")
public class AgenciaResource {
	
	@Autowired
	private AgenciaRepository agenciaRepository;
	
	@PostMapping("/criar")
	public Agencia adicionar(@Valid @RequestBody Agencia agencia) {
		return agenciaRepository.save(agencia);
	}
	
	@GetMapping("/listar")
	public Iterable<Agencia> listarAgencias() {
		Iterable<Agencia> listarAgencias = agenciaRepository.findAll();
		return listarAgencias;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Agencia> buscar(@PathVariable long id){
		Agencia agencia = agenciaRepository.findById(id);
		if (agencia == null) 
		    throw new RegistroNaoEncontrado("Agência não encontrada para o ID: " + id);
		return ResponseEntity.ok(agencia);
	}
	
	@PutMapping("/atualizar/{id}")
	public ResponseEntity<Agencia> atualizar(@PathVariable long id, @Valid @RequestBody Agencia agencia) {
		Agencia agencia_encontrada = agenciaRepository.findById(id);
		
		if (agencia_encontrada == null)
		    throw new RegistroNaoEncontrado("Agência não encontrada para o ID: " + id);
		
		BeanUtils.copyProperties(agencia, agencia_encontrada, "id");
		
		agencia_encontrada = agenciaRepository.save(agencia_encontrada);
		return ResponseEntity.ok(agencia_encontrada);
	}
	
	@DeleteMapping("/excluir/{id}")
	public ResponseEntity<Void> excluir(@PathVariable long id) {
		Agencia agencia = agenciaRepository.findById(id);
		
		if (agencia == null)
		    throw new RegistroNaoEncontrado("Agência não encontrada para o ID: " + id);
		
		agenciaRepository.delete(agencia);
		return ResponseEntity.noContent().build();
	}
}