package com.bancos.bancoapi.resources;


import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancos.bancoapi.models.Banco;
import com.bancos.bancoapi.repository.BancoRepository;
import com.bancos.bancoapi.exception.RegistroNaoEncontrado;


@RestController
@RequestMapping(value="/api/banco")
public class BancosResource {
	
	@Autowired
	private BancoRepository bancoRepository;
	
	@PostMapping("/criar")
	public Banco adicionar(@Valid @RequestBody Banco banco) {
		return bancoRepository.save(banco);
	}


	@GetMapping("/listar")
	public Iterable<Banco> listarBancos() {
		Iterable<Banco> listarBancos = bancoRepository.findAll();
		return listarBancos;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Banco> buscar(@PathVariable long id){
		Banco banco = bancoRepository.findById(id);
		if (banco == null) 
		    throw new RegistroNaoEncontrado("Banco não encontrado para o ID: " + id);
		return ResponseEntity.ok(banco);
	}
	
	@PutMapping("/atualizar/{id}")
	public ResponseEntity<Banco> atualizar(@PathVariable long id, @Valid @RequestBody Banco banco) {
		Banco banco_encontrado = bancoRepository.findById(id);
		
		if (banco_encontrado == null)
		    throw new RegistroNaoEncontrado("Banco não encontrado para o ID: " + id);
		
		BeanUtils.copyProperties(banco, banco_encontrado, "id");
		
		banco_encontrado = bancoRepository.save(banco_encontrado);
		return ResponseEntity.ok(banco_encontrado);
	}
	
	//@PreAuthorize("hasRole('ADMIN')")
	
	@DeleteMapping("/excluir/{id}")
	public ResponseEntity<Void> excluir(@PathVariable long id) {
		Banco banco = bancoRepository.findById(id);
		
		if (banco == null)
		    throw new RegistroNaoEncontrado("Banco não encontrado para o ID: " + id);
		
		bancoRepository.delete(banco);
		return ResponseEntity.noContent().build();
	}
}