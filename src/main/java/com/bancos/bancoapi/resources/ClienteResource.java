package com.bancos.bancoapi.resources;


import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancos.bancoapi.models.Cliente;
import com.bancos.bancoapi.repository.ClienteRepository;
import com.bancos.bancoapi.exception.RegistroNaoEncontrado;


@RestController
@RequestMapping(value="/api/cliente")
public class ClienteResource {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@PostMapping("/criar")
	public Cliente adicionar(@Valid @RequestBody Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	@GetMapping("/listar")
	public Iterable<Cliente> listarClientes() {
		Iterable<Cliente> listarClientes = clienteRepository.findAll();
		return listarClientes;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> buscar(@PathVariable long id){
		Cliente cliente = clienteRepository.findById(id);
		if (cliente == null) 
		    throw new RegistroNaoEncontrado("Cliente não encontrado para o ID: " + id);
		return ResponseEntity.ok(cliente);
	}
	
	@PutMapping("/atualizar/{id}")
	public ResponseEntity<Cliente> atualizar(@PathVariable long id, @Valid @RequestBody Cliente cliente) {
		Cliente cliente_encontrado = clienteRepository.findById(id);
		
		if (cliente_encontrado == null)
		    throw new RegistroNaoEncontrado("Cliente não encontrado para o ID: " + id);
		
		BeanUtils.copyProperties(cliente, cliente_encontrado, "id");
		
		cliente_encontrado = clienteRepository.save(cliente_encontrado);
		return ResponseEntity.ok(cliente_encontrado);
	}
	
	@DeleteMapping("/excluir/{id}")
	public ResponseEntity<Void> excluir(@PathVariable long id) {
		Cliente cliente = clienteRepository.findById(id);
		
		if (cliente == null)
		    throw new RegistroNaoEncontrado("Cliente não encontrado para o ID: " + id);
		
		clienteRepository.delete(cliente);
		return ResponseEntity.noContent().build();
	}
}