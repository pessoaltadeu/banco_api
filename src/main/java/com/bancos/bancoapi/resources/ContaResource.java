package com.bancos.bancoapi.resources;


import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancos.bancoapi.models.Conta;
import com.bancos.bancoapi.repository.ContaRepository;
import com.bancos.bancoapi.exception.RegistroNaoEncontrado;


@RestController
@RequestMapping(value="/api/conta")
public class ContaResource {
	
	@Autowired
	private ContaRepository contaRepository;
	
	@PostMapping("/criar")
	public Conta adicionar(@Valid @RequestBody Conta conta) {
		return contaRepository.save(conta);
	}
	
	@GetMapping("/listar")
	public Iterable<Conta> listarContas() {
		Iterable<Conta> listarContas = contaRepository.findAll();
		return listarContas;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Conta> buscar(@PathVariable long id){
		Conta conta = contaRepository.findById(id);
		if (conta == null) 
		    throw new RegistroNaoEncontrado("Conta não encontrada para o ID: " + id);
		return ResponseEntity.ok(conta);
	}
	
	@PutMapping("/atualizar/{id}")
	public ResponseEntity<Conta> atualizar(@PathVariable long id, @Valid @RequestBody Conta conta) {
		Conta conta_encontrada = contaRepository.findById(id);
		
		if (conta_encontrada == null)
		    throw new RegistroNaoEncontrado("Conta não encontrada para o ID: " + id);
		
		BeanUtils.copyProperties(conta, conta_encontrada, "id");
		
		conta_encontrada = contaRepository.save(conta_encontrada);
		return ResponseEntity.ok(conta_encontrada);
	}
	
	@DeleteMapping("/excluir/{id}")
	public ResponseEntity<Void> excluir(@PathVariable long id) {
		Conta conta = contaRepository.findById(id);
		
		if (conta == null)
		    throw new RegistroNaoEncontrado("Conta não encontrada para o ID: " + id);
		
		contaRepository.delete(conta);
		return ResponseEntity.noContent().build();
	}
}