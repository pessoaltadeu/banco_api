package com.bancos.bancoapi.resources;

import java.awt.PageAttributes.MediaType;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancos.bancoapi.exception.RegistroNaoEncontrado;
import com.bancos.bancoapi.exception.OperacaoNaoPermitida;
import com.bancos.bancoapi.models.Conta;
import com.bancos.bancoapi.models.OperacaoBancaria;
import com.bancos.bancoapi.repository.OperacaoBancariaRepository;
import com.bancos.bancoapi.repository.ContaRepository;
import com.bancos.bancoapi.resources.ContaResource;



@RestController
@RequestMapping(value="/api/movimentar_conta")
public class OperacaoBancariaResource {
	
	@Autowired
	private OperacaoBancariaRepository operacaoBancariaRepository;
	
	@Autowired
	private ContaRepository contaRepository;
	
	
	@PostMapping("/sacar")
	public OperacaoBancaria operacao_saque(@Valid @RequestBody OperacaoBancaria operacaoBancaria) {
		
		Conta conta1 =  new Conta();
		conta1.setId(operacaoBancaria.getConta().getId());
		long i = conta1.getId();
		Conta conta = contaRepository.findById(i);
		double saldo = conta.getSaldo();
		double valor = operacaoBancaria.getValor();
		if (saldo < valor)
		    throw new OperacaoNaoPermitida("Saldo insuficiente!");
		conta.setSaldo(saldo-valor);
		contaRepository.save(conta);
		return operacaoBancariaRepository.save(operacaoBancaria);
	}
	
	@PostMapping("/depositar")
	public OperacaoBancaria operacao_deposito(@Valid @RequestBody OperacaoBancaria operacaoBancaria) {
		
		Conta conta1 =  new Conta();
		conta1.setId(operacaoBancaria.getConta().getId());
		long i = conta1.getId();
		Conta conta = contaRepository.findById(i);
		double saldo = conta.getSaldo();
		double valor = operacaoBancaria.getValor();
		conta.setSaldo(saldo+valor);
		contaRepository.save(conta);
		return operacaoBancariaRepository.save(operacaoBancaria);
	}
	
	@PostMapping("/transferir")
	public OperacaoBancaria operacao_transferencia(@Valid @RequestBody OperacaoBancaria operacaoBancaria) {
		
		Conta conta1 =  new Conta();
		conta1.setId(operacaoBancaria.getConta().getId());
		long i = conta1.getId();
		Conta conta_transfere = contaRepository.findById(i);
		double saldo_conta_transfere = conta_transfere.getSaldo();
		
		double valor = operacaoBancaria.getValor();
		
		if (saldo_conta_transfere < valor)
		    throw new OperacaoNaoPermitida("Saldo insuficiente!");
		
		conta_transfere.setSaldo(saldo_conta_transfere - valor);
		contaRepository.save(conta_transfere);
		
		
		Conta conta2 =  new Conta();
		conta2.setId(operacaoBancaria.getConta_transferencia().getId());
		long ii = conta2.getId();
		Conta conta_recebe = contaRepository.findById(ii);
		double saldo_conta_recebe = conta_recebe.getSaldo();
		
		conta_recebe.setSaldo(saldo_conta_recebe + valor);
		contaRepository.save(conta_recebe);
		return operacaoBancariaRepository.save(operacaoBancaria);
	}
	
	@GetMapping("/listar")
	public Iterable<OperacaoBancaria> extrato() {
		Iterable<OperacaoBancaria> operacoes = operacaoBancariaRepository.findAll();
		return operacoes;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<OperacaoBancaria> buscar(@PathVariable long id){

		OperacaoBancaria operacaoBancaria = operacaoBancariaRepository.findById(id);
		if (operacaoBancaria == null) 
		    throw new RegistroNaoEncontrado("Operação Bancária não encontrada para o ID: " + id);
		return ResponseEntity.ok(operacaoBancaria);
	}
}