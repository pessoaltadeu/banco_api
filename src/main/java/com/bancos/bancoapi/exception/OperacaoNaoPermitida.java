package com.bancos.bancoapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.ACCEPTED)
public class OperacaoNaoPermitida extends RuntimeException {
	
	public OperacaoNaoPermitida(String message) {
	    super(message);
	  }
	
}
