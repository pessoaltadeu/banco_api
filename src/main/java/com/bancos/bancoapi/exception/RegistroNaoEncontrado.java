package com.bancos.bancoapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RegistroNaoEncontrado extends RuntimeException {
	
	public RegistroNaoEncontrado(String message) {
	    super(message);
	  }
	
}
