package com.bancos.bancoapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bancos.bancoapi.models.Banco;


@Repository
public interface BancoRepository extends JpaRepository<Banco, Long> {
	
	Banco findById(long id);
	
}