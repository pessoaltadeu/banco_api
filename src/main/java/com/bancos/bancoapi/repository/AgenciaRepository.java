package com.bancos.bancoapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancos.bancoapi.models.Agencia;
import com.bancos.bancoapi.models.Banco;

public interface AgenciaRepository extends JpaRepository<Agencia, Long> {
	
	Agencia findById(long id);
	
}
