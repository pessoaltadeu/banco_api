package com.bancos.bancoapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancos.bancoapi.models.Conta;

public interface ContaRepository extends JpaRepository <Conta, Long> {

	Conta findById(long id);
	
}
