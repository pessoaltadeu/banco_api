package com.bancos.bancoapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancos.bancoapi.models.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	Cliente findById(long id);

}
