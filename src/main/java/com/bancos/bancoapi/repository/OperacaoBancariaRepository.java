package com.bancos.bancoapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import com.bancos.bancoapi.exception.RegistroNaoEncontrado;
import com.bancos.bancoapi.models.Conta;
import com.bancos.bancoapi.models.OperacaoBancaria;

public interface OperacaoBancariaRepository extends JpaRepository<OperacaoBancaria, Long> {
	
	OperacaoBancaria findById(long id);

}
