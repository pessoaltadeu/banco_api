# README #

Desafio: Criar uma API REST FULL que simule operações bancárias.

### 1. Baixe o projeto a partir deste repositório ###
$ git clone https://tadeuc@bitbucket.org/pessoaltadeu/banco_api.git

### 2. Crie uma base de dados MySql ###
'''bash
create database banco_api;
'''

obs: Caso queira usar outro nome pra o banco de dados altere o arquivo application.properties substituindo "banco_api" por o nome do seu banco na seguinte linha:

spring.datasource.url = jdbc:mysql://localhost:3306/banco_api?useSSL=false

### 3. Configure o usuário e senha do seu banco no arquivo application.properties ###

spring.datasource.username = root
spring.datasource.password = beloriso

### 4. Ainda no arquivo "application.properties" configure o acesso do usuário cliente ###
security.oauth2.client.scope=password
security.oauth2.client.client-id=usuario_api_banco
security.oauth2.client.client-secret=acesso_api

### 5. Rode o projeto ###
utilize uma IDE e execute o arquivo BancoapiAplication.java
a API irá rodar em <http://localhost:8080>
obs: Costume de usar python... não lembrei de fazer gerar o .jar

### 6. Usuários de acesso ###

Temos apenas 2 usuários "inMemory" que serão utilizados para acessar a api:
username1: usuario
senha: 123456
username2: admin
senha: 123456

### 7. Tabelas 
	banco
	agencia
	conta
	cliente
	
### 7. Explore a API ###
Para as tabelas banco, agencia, conta, cliente faça as requisições da seguinte maneira:
-> POST: /api/<nome_tabela>/criar  # para criar um registro
-> GET: /api/<nome_tabela>/listar  # para listar todos os itens
-> GET: /api/<nome_tabela>/<id>  # para buscar apenas 1 ítem
-> PUT: /api/<nome_tabela>/atualizar/<id>  # para salvar informações em 1 registro.
-> DELETE: /api/<nome_tabela>/excluir/<id> # para excluir um registro do banco.

Para as operações financeiras: sacar, depositar, tranferir, listar, use:

-> POST: /api/movimentar_conta/<operação>

### Exemplos de de body para os métodos POST ###
# criar um banco:
{
	"codigo": "104",
	"nome": "Caixa Econômica"
}

# criar uma agencia
{
	"banco": {"id": 1},
    "numero_agencia": "2404"
}

# criar um cliente
{
	"nome": "Tadeu Cavalcante",
	"cpf": "123.456.789-00"
}

# criar uma conta
{
    "cliente": 1,
    "agencia": 1,
    "tipo": "Poupança",
    "numero_conta": "1234,
    "saldo": 300.0
}

# uma operação bancária
-Saques e depósitos
{
	"conta": {"id":1},
	"valor": 40,
	"tipo_operacao": "saque"
}

-Transferência
{
	"conta": {"id":1},
	"conta_transferencia": {"id":2},
	"valor": 40,
	"tipo_operacao": "saque"
}